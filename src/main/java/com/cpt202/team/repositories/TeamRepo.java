package com.cpt202.team.repositories;

import com.cpt202.team.models.Team;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamRepo extends JpaRepository<Team, Integer >{
    
}
